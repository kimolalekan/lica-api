"use strict";

const fetch = require("node-fetch");
const moment = require("moment");
const logger = require("./log");

const url = process.env.SMS_URL;
const user = process.env.SMS_USER;
const pass = process.env.SMS_PASS;

const sms = async (from, to, message) => {
  let uri = `${url}?username=${user}&password=${pass}&sender=${from}&message=${message}&flash=0&recipients=${to}`;
  const query = await fetch(uri);
  const res = await query.json();
  return res;
};

const sendSMS = (req, res) => {
  let message = encodeURIComponent(req.query.message);
  let from = req.query.from;
  let to = req.query.to;

  sms(from, to, message)
    .then(response => {
      console.log(response);

      if (response == 100) {
        res.send({ success: true });
      } else {
        let time = moment().format("MMM D, YYYY @ h:mm:ss A");
        let error = `Multitexter- error(${response}) -  ${time}`;
        logger.error(error);
        res.send({ success: false });
      }
    })
    .catch(err => {
      let time = moment().format("MMM D, YYYY @ h:mm:ss A");
      let error = `${err} -  ${time}`;
      logger.error(error);
    });
};

const localSMS = (from, to, message) => {
  message = encodeURIComponent(message);

  sms(from, to, message)
    .then(response => {
      if (response == 100) {
        return { success: true };
      } else {
        let time = moment().format("MMM D, YYYY @ h:mm:ss A");
        let error = `Multitexter- error(${response}) -  ${time}`;
        logger.error(error);
        return { success: false };
      }
    })
    .catch(err => {
      let time = moment().format("MMM D, YYYY @ h:mm:ss A");
      let error = `${err} -  ${time}`;
      logger.error(error);
    });
};

module.exports = { sendSMS, localSMS };
