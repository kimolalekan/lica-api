"use strict";

const Sequelize = require("sequelize");
const sequelize = require("../config/database.js");
const { guid, slug } = require("../utils");

const User = sequelize.define("user", {
  id: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: guid()
  },
  firstName: Sequelize.STRING,
  lastName: Sequelize.STRING,
  email: { type: Sequelize.STRING, unique: true },
  phone: Sequelize.STRING,
  password: Sequelize.STRING,
  account: Sequelize.STRING,
  accountNo: Sequelize.STRING,
  bvn: Sequelize.STRING,
  verified: Sequelize.BOOLEAN
});

const Loan = sequelize.define("loan", {
  id: {
    type: Sequelize.STRING,
    primaryKey: true,
    defaultValue: slug()
  },
  amount: Sequelize.DECIMAL(10, 2),
  duration: Sequelize.STRING,
  transactionId: Sequelize.STRING,
  user: Sequelize.STRING,
  status: Sequelize.BOOLEAN
});

const Transaction = sequelize.define("transaction", {
  id: {
    type: Sequelize.STRING,
    primaryKey: true,
    defaultValue: slug()
  },
  transactionId: Sequelize.STRING,
  referenceId: Sequelize.STRING,
  paystack: Sequelize.STRING,
  remita: Sequelize.STRING,
  user: Sequelize.STRING
});

const Analytics = sequelize.define("analytic", {
  uid: Sequelize.STRING,
  apikey: Sequelize.STRING,
  agent: Sequelize.STRING,
  url: Sequelize.STRING,
  ip: Sequelize.STRING,
  method: Sequelize.STRING,
  status: Sequelize.STRING,
  duration: Sequelize.STRING,
  day: Sequelize.STRING
});

module.exports = {
  User,
  Loan,
  Transaction,
  Analytics
};
