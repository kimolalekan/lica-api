### Lica API

Install global dependencies

```
$ npm i -g yarn nodemon pm2
```

Install local dependencies

```
$ yarn
```

Start development

```
yarn dev
```

Start production

```
yarn production
```

Start app

```
yarn stop
```
