const path = require("path");
const winston = require("winston");
const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  exitOnError: false,
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({
      filename: path.join(__dirname, "../../log/error.log"),
      level: "error"
    })
  ]
});

// logger.add(
//   new winston.transports.Console({
//     format: winston.format.simple()
//   })
// );
module.exports = logger;
