"use strict";

const bcrypt = require("bcrypt");
const moment = require("moment");
const sequelize = require("../config/database");
const { User } = require("../model");
const { code } = require("../utils");
const { localSMS } = require("./sms");
const logger = require("./log");

//User exists
const userExist = (req, res) => {
  let phone = req.body.phone;

  sequelize
    .sync()
    .then(() => User.count({ where: { phone: phone } }))
    .then(user => {
      if (user === 0) {
        const newCode = code();
        const message = `Your Lica verification code is ${newCode}`;
        // localSMS("Lica", phone, message);
        res.send({ status: false, code: newCode });
      } else {
        res.status(400).send({ status: true, error: "User exists" });
      }
    })
    .catch(err => {
      let time = moment().format("MMM D, YYYY @ h:mm:ss A");
      let error = `${err} -  ${time}`;
      logger.error(error);
    });
};

//Add user
const newUser = (req, res) => {
  const formData = req.body;
  let email = formData.email;
  //Hash the password
  const salt = bcrypt.genSaltSync(10);
  const pass = bcrypt.hashSync(formData.password, salt);
  formData.password = pass;

  sequelize
    .sync()
    .then(() => User.count({ where: { email: email } }))
    .then(user => {
      if (!user) {
        sequelize
          .sync()
          .then(() => User.create(formData))
          .then(user => {
            let new_user = user.get();

            //Delete sensitive data
            delete new_user.password;
            delete new_user.updatedAt;

            //Now send the data
            res.send(new_user);
          })
          .catch(err => console.log(err));
      } else {
        res.status(400).send({ success: false, error: "duplicate" });
      }
    })
    .catch(err => console.log(err));
};

//List all users
const getUsers = (req, res) => {
  sequelize
    .sync()
    .then(() => User.findAll())
    .then(users => {
      res.send(users);
    })
    .catch(err => console.log(err));
};

//Get single user data via id
const getUser = (req, res) => {
  const id = req.params.id;

  sequelize
    .sync()
    .then(() => User.findOne({ where: { id: id } }))
    .then(data => {
      if (data) {
        let user = data.get();

        //Delete sensitive data
        delete user.password;
        delete user.updatedAt;

        res.send(user);
      } else {
        res.status(400).send({ success: false, error: "Unable to get user" });
      }
    })
    .catch(err => console.log(err));
};

//Update user
const updateUser = (req, res) => {
  let userData = req.body;

  if (userData.password) {
    //Hash the password
    let salt = bcrypt.genSaltSync(10);
    let pass = bcrypt.hashSync(userData.password, salt);
    userData.password = pass;
  } else {
    delete userData.password;
  }

  sequelize
    .sync()
    .then(() => User.update(userData, { where: { uid: userData.uid } }))
    .then(user => {
      if (user[0]) {
        res.send({ success: true, status: "updated" });
      } else {
        res.send({ success: false, status: "Not updated" });
      }
    })
    .catch(err => console.log(err));
};

//Check user data and respond with details
const userLogin = (req, res) => {
  const formData = req.body;
  sequelize
    .sync()
    .then(() => User.findOne({ where: { email: formData.email } }))
    .then(data => {
      if (!data) {
        res.send({ error: "Incorrect email or password!" });
      } else {
        let user = data.get();
        let pass = bcrypt.compareSync(formData.password, user.password);

        if (pass) {
          //Delete sensitive data
          delete user.password;
          delete user.updatedAt;
          res.send(user);
        } else {
          res.status(400).send({ error: "Incorrect email or password!" });
        }
      }
    })
    .catch(err => console.log(err));
};

//Forgot password code
const forgotPass = (req, res) => {
  const formData = req.body;
  let email = formData.email;
  sequelize
    .sync()
    .then(() => User.count({ where: { email: email } }))
    .then(user => {
      if (user) {
        let codes = code();
        //Send mail
        // sendMail("", email, "Filmdrive password reset", "forgot", codes);
        res.send({ code: codes });
      } else {
        res.send({ success: false });
      }
    });
};

//Reset password
const resetPass = (req, res, next) => {
  const formData = req.body;
  let email = formData.email;

  const salt = bcrypt.genSaltSync(10);
  const pass = bcrypt.hashSync(formData.password, salt);
  formData.password = pass;
  delete formData.email;

  sequelize
    .sync()
    .then(() => User.update(formData, { where: { email: email } }))
    .then(() => {
      res.send({ success: true });
    })
    .catch(err => console.log(err));
};

module.exports = {
  userExist,
  newUser,
  updateUser,
  getUsers,
  getUser,
  userLogin,
  forgotPass,
  resetPass
};
