"use strict";

const slug = () => {
  return (
    Math.random()
      .toString(36)
      .substring(2, 7) +
    Math.random()
      .toString(36)
      .substring(2, 7)
  );
};

const code = () => {
  let code = Math.random() * (1000000 - 10000) + 10000;
  code = Math.round(code);

  return code;
};

const guid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    let r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

const toUnique = (a, b, c) => {
  b = a.length;
  while ((c = --b)) while (c--) a[b] !== a[c] || a.splice(c, 1);
  return a;
};

const removeItem = (array, element) => {
  let index = array.indexOf(element);
  if (index >= 1) {
    array.splice(index, 1);
  }
  return array;
};

const requiredFields = (object, array) => {
  let result;
  array.forEach(item => {
    console.log(object[item]);

    object[item] ? (result = `${item} is required`) : (result = false);
  });
  return result;
};

const objToArray = obj => {
  let arr = [];

  for (var i in obj) {
    arr.push(obj[i]);
  }
  return arr;
};

module.exports = {
  slug,
  code,
  guid,
  toUnique,
  removeItem,
  requiredFields,
  objToArray
};
