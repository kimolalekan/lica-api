"use strict";

const fs = require("fs");
const path = require("path");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const morgan = require("morgan");
const app = express();
require("dotenv").config();
const { objToArray } = require("./utils");
const {
  userExist,
  newUser,
  updateUser,
  getUsers,
  getUser,
  userLogin,
  forgotPass,
  resetPass,
  verifyProfile,
  getBanks,
  standingOrder,
  sendSMS
} = require("./api");

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "../log/access.log"),
  {
    flags: "a"
  }
);

// setup the logger
app.use(
  morgan(
    `:remote-addr - :remote-user :method :status :url :referrer :user-agent :response-time ms :date[web]`,
    {
      stream: accessLogStream
    }
  )
);

//Handle wrong apikey => Uncomment this out, if you want to use the API with authentication.
app.use((req, res, next) => {
  let apiKeys = [
    process.env.ANDROID_API_KEY,
    process.env.IOS_API_KEY,
    process.env.WEB_API_KEY
  ];
  let key = req.headers.apikey;
  if (apiKeys.indexOf(key) == -1) {
    res.status(400).send({
      error: "Unauthorized access",
      help: "Please check the docs."
    });
  } else {
    next();
  }
});

//Track api usage
// app.use(analytics);

//User endpoints
// app.route("/").get(Home);
app.route("/api/verify/new").post(userExist);
app.route("/api/verify").get(verifyProfile);
app.route("/api/banks").get(getBanks);
app.route("/api/standing-order").post(standingOrder);
app.route("/api/sms").get(sendSMS);
app.route("/api/user/create").post(newUser);
app.route("/api/user/update").post(updateUser);
app.route("/api/users").get(getUsers);
app.route("/api/user/:id").get(getUser);
app.route("/api/user/login").post(userLogin);
app.route("/api/user/forgot").post(forgotPass);
app.route("/api/user/reset").post(resetPass);

//Analytics endpoint
// app.route("/api/analytics").get(analyticsAPI);

//Handle error
// app.use(function(err, req, res) {
//   console.error(err.stack);
//   res.status(500).send({
//     error: "Something broken, but it is fixable.",
//     help: "Please check the docs."
//   });
// });

//Handle 404
app.use((req, res) => {
  res.status(404).send({
    error: "Something broken beyond fixing!",
    help: "Please check the docs."
  });
});

app.listen(process.env.API_PORT);
