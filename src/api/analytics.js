"use strict";

const moment = require("moment");
const MobileDetect = require("mobile-detect");
const sequelize = require("../config/database");
const { Analytics } = require("../model");
const start = new Date();

//New log
const analytics = (req, res, next) => {
  let md, ua, time, api_log;
  md = new MobileDetect(req.headers["user-agent"]);
  ua = md.ua;
  time = new Date() - start;
  time = time / 1000;

  api_log = {
    apikey: req.headers.apikey,
    url: req.url,
    ip: req.ip,
    method: req.method,
    status: res.statusCode,
    duration: time + " ms",
    day: moment().format("YYYY-MM-DD"),
    agent: ua
  };

  sequelize
    .sync()
    .then(() => Analytics.create(api_log))
    .catch(err => console.log(err));
  next();
};

//Get analytics API
const analyticsAPI = (req, res) => {
  let date = new Date();

  const begin = moment()
    .year(date.getFullYear())
    .month(date.getMonth())
    .date(1)
    .format("YYYY-MM-DD");

  const end = moment()
    .year(date.getFullYear())
    .month(date.getMonth() + 1)
    .date(1)
    .format("YYYY-MM-DD");

  sequelize
    .query(
      `SELECT count(duration) as duration, day FROM analytics WHERE createdAt >= '${begin} 00:00:00' AND createdAt < '${end} 23:59:59' GROUP BY day`,
      {
        model: Analytics,
        raw: true
      }
    )
    .then(Analytics => {
      let analytics = {},
        days = [],
        durations = [];

      Analytics.forEach(item => {
        days.push(item.day);
        durations.push(item.duration);
      });

      analytics = {
        day: days,
        duration: durations
      };

      res.send({ success: true, data: analytics });
    })
    .catch(err => console.log(err));
};

module.exports = { analytics, analyticsAPI };
