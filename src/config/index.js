module.exports = {
  api: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    pass: process.env.DB_PASS,
    db: process.env.DB_NAME,
    port: process.env.API_PORT
  },
  key: {
    dev: "ae4a3b1f-86bd-4b09-b592-fa35a1cd8a60",
    production: "b4fa132f-0582-43d1-98ee-f35dcd102664",
    android: "510185c9-2964-46c2-bab6-8845d9867d50",
    ios: "8cfdec5a-e6e3-4208-97b7-71157f7bf405"
  }
};
