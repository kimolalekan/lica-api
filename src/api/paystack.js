"use strict";

const fetch = require("node-fetch");
const moment = require("moment");
const logger = require("./log");
const { code } = require("../utils");

const PAYSTACK_BANK = process.env.PAYSTACK_BANK;
const PAYSTACK_SECRET = process.env.PAYSTACK_SECRET;
const PAYSTACK_TRANSACTION = process.env.PAYSTACK_TRANSACTION;

//Paystack functions
const verifyBVN = async bvn => {
  let uri = `${PAYSTACK_BANK}resolve_bvn/${bvn}`;
  let query = await fetch(uri, {
    headers: {
      Authorization: `Bearer ${PAYSTACK_SECRET}`
    }
  }).catch(err => {
    let time = moment().format("MMM D, YYYY @ h:mm:ss A");
    log.error(`${time} - ${err}`);
  });

  let res = await query.json();
  return res;
};

const verifyAccount = async (account, code) => {
  let uri = `${PAYSTACK_BANK}resolve?account_number=${account}&bank_code=${code}`;
  let query = await fetch(uri, {
    headers: {
      Authorization: `Bearer ${PAYSTACK_SECRET}`
    }
  }).catch(err => {
    let time = moment().format("MMM D, YYYY @ h:mm:ss A");
    log.error(`${time} - ${err}`);
  });
  let res = await query.json();
  return res;
};

const bankAccount = async account => {
  let uri = `${PAYSTACK_BANK}?perPage=100`;
  let query = await fetch(uri, {
    headers: {
      Authorization: `Bearer ${PAYSTACK_SECRET}`
    }
  }).catch(err => console.log(err));
  let res = await query.json();

  return res;
};

const initializeTransaction = async account => {
  let uri = `${PAYSTACK_TRANSACTION}initialize`;
  let query = await fetch(uri, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${PAYSTACK_SECRET}`,
      "content-type": "application/json"
    },
    body: account
  }).catch(err => console.log(err));

  let res = await query.json();
  return res;
};

//Paystack middlewares

const verifyProfile = (req, res) => {
  const bvn = req.query.bvn;
  const account = req.query.account;
  const accountCode = req.query.code;

  verifyBVN(bvn)
    .then(bvnResult => {
      if (bvnResult.status) {
        verifyAccount(account, accountCode)
          .then(accountResult => {
            const data = { bvnResult, accountResult };
            res.send(data);
          })
          .catch(err => {
            let time = moment().format("MMM D, YYYY @ h:mm:ss A");
            let error = `${err} -  ${time}`;
            logger.error(error);
          });
      } else {
        res.send({ error: "Unable to get verify BVN" });
      }
    })
    .catch(err => {
      let time = moment().format("MMM D, YYYY @ h:mm:ss A");
      let error = `${err} -  ${time}`;
      logger.error(error);
    });
};

const getBanks = (req, res) => {
  bankAccount()
    .then(bank => {
      let data = bank.data;
      let banks = [];
      data.forEach(item => {
        let label = item.name;
        let value = item.code;
        let resp = { label, value };
        banks.push(resp);
      });

      res.send(banks);
    })
    .catch(err => {
      let time = moment().format("MMM D, YYYY @ h:mm:ss A");
      let error = `${err} -  ${time}`;
      logger.error(error);
    });
};

const standingOrder = (req, res) => {
  let callback = process.env.PAYSTACK_CALLBACK;
  let account = req.body;
  account.reference = `lk${code()}`;
  account.callback_url = `${callback}?ref=${account.reference}`;
  account = JSON.stringify(req.body);

  initializeTransaction(account)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      let time = moment().format("MMM D, YYYY @ h:mm:ss A");
      let error = `${err} -  ${time}`;
      logger.error(error);
    });
};

module.exports = { verifyProfile, getBanks, standingOrder };
