"use strict";

const formidable = require("formidable");
const path = require("path");

const getEXT = file => {
  file = file.split(".");
  let count = file.length;
  count = count - 1;
  return file[count];
};

const isImage = type => {
  if (
    type === "jpg" ||
    type === "JPG" ||
    type === "jpeg" ||
    type === "JPEG" ||
    type === "png" ||
    type === "PNG" ||
    type === "gif"
  ) {
    return true;
  } else {
    return false;
  }
};

const isDoc = type => {
  if (
    type === "pdf" ||
    type === "xls" ||
    type === "xlsx" ||
    type === "ppt" ||
    type === "pptx" ||
    type === "doc" ||
    type === "docx"
  ) {
    return true;
  } else {
    return false;
  }
};

const uploadImage = (req, res, next) => {
  const id = req.params.id;
  const form = new formidable.IncomingForm();
  form.parse(req);

  form.on("fileBegin", function(name, file) {
    let type = getEXT(file.name);

    if (isImage(type)) {
      let newFile = `${Date.now()}.${type}`;
      file.path = path.resolve("./src/storage/images/", newFile);

      res.send({ success: true, file: newFile });
    } else {
      res.send({ success: false, error: "Please upload valid image" });
    }
  });
};

const uploadFile = (req, res, next) => {
  const id = req.params.id;
  const form = new formidable.IncomingForm();
  form.parse(req);

  form.on("fileBegin", function(name, file) {
    let type = getEXT(file.name);

    if (isDoc(type)) {
      let newFile = `${Date.now()}.${type}`;
      file.path = path.resolve("./src/storage/documents/", newFile);

      res.send({ success: true, file: newFile });
    } else {
      res.send({ success: false, error: "Please upload valid document" });
    }
  });
};

module.exports = {
  uploadImage,
  uploadFile
};
