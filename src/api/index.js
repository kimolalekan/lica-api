"use strict";

const {
  userExist,
  newUser,
  updateUser,
  getUsers,
  getUser,
  userLogin,
  forgotPass,
  resetPass
} = require("./user");

// const { analytics, analyticsAPI } = require("./analytics");
const { uploadImage, uploadFile } = require("./upload");
const { verifyProfile, getBanks, standingOrder } = require("./paystack");
const { sendSMS } = require("./sms");

module.exports = {
  userExist,
  newUser,
  updateUser,
  getUsers,
  getUser,
  userLogin,
  forgotPass,
  resetPass,
  uploadImage,
  uploadFile,
  verifyProfile,
  getBanks,
  standingOrder,
  sendSMS
};
